#image processing class with image processing and flitering

import math
from PIL import Image, ImageFilter, ImageTk

class ImageProcessing:
    def __init__(self, w, h):
        self.width, self.heigh = w, h
        self.name = "noname.jpg"
        self.image = Image.new("RGB",(self.width, self.height))
    #end __init__

    def __init__(self, name):
        self.name = name
        self.image = Image.open(self.name)
        self.width, self.height = self.image.width, self.image.height
    #end __init__

    #some IP methods
    def invert(self, red = True, green = True, blue = True):
        #open an empty image
        newImage = Image.new("RGB",(self.width, self.height))

        #traverse the image to invert (r,g,b)
        for i in range(self.width):
            for j in range(self.height):
                r,g,b = self.image.getpixel((i,j))
                if red:
                    newR = 255 - r
                else:
                    newR = r
                #end Red

                if green:
                    newG = 255 - g
                else:
                    newG = g
                #end Green

                if blue:
                    newB = 255 - b
                else:
                    newB = b
                #end Blue
                newImage.putpixel((i,j), (newR, newG, newB))

        return newImage

        #save the new image
    #end invert

    def emboss(self):
        return self.image.filter(ImageFilter.EMBOSS)
    #end emboss      

    def blur(self, radius = 3):
        return self.image.filter(ImageFilter.GaussianBlur(radius))
    #end emboss      
