from gimpfu import *

def new_image_layer(image, layer, file, alpha):
    gimp.progress_init("Loading " + str(file) + "...")
    
    try:
        src = None
        if(file.lower().endswith((".png"))):
            src = pdb.file_png_load(file, file)
        if(file.lower().endswith((".jpg"))):
            src = pdb.file_jpeg_load(file, file)
        
        if(src is None):
            pdb.gimp_message("Error loading Image")
        else :
            new = gimp.Layer(image, "New Image layer", image.width, image.height, layer.type, alpha, layer.mode)
            image.add_layer(new, 0)               
        
            target = src.layers[0]
            pdb.gimp_edit_copy(target)
            pdb.gimp_edit_paste(new, True)
            new.flush()
            new.merge_shadow(True)
            new.update(0, 0, image.width, image.height)
        
    except Exception as err:
        pdb.gimp_message("Unexpected error: " + str(err))

def new_text_layer(image, drawable, layer, t_x, t_y, text, alpha, color, font):
    gimp.progress_init("Loading " + str(text) + "...")
    gimp.set_foreground(color)
    newTextLayer = pdb.gimp_text_fontname(image, None, t_x, t_y, text, 10, True, 40, PIXELS, "Sans")
	

def poster_script(image, drawable, layer, background, foreground, foreground_alpha, subject, subject_alpha, title, title_x, title_y, sub_title, sub_title_x, sub_title_y, poster):

    gimp.progress_init("Generating " + str(poster) + " ...")

    pdb.gimp_message("Image : " + str(image))
    pdb.gimp_message("Drawable : " + str(drawable))
    pdb.gimp_message("Layer : " + str(layer))
    pdb.gimp_message("Background :" + str(background))
    pdb.gimp_message("Foreground : " + str(foreground))
    pdb.gimp_message("Alpha : " + str(foreground_alpha))
    pdb.gimp_message("Subject : " + str(subject))
    pdb.gimp_message("Alpha : " + str(subject_alpha))
    pdb.gimp_message("Title : " + str(title))
    pdb.gimp_message("Subtitle : " + str(sub_title))
    pdb.gimp_message("Poster : " + str(poster))

    new_image_layer(image, layer, background, 100)
    new_image_layer(image, layer, background, foreground_alpha)
    new_image_layer(image, layer, subject, subject_alpha)

    new_text_layer(image, drawable, layer, title_x, title_y, title, 100, (255,255,255), "Sans")
    new_text_layer(image, drawable, layer, sub_title_x, sub_title_y, sub_title, 100, (255,255,255), "Sans")

    gimp.progress_init("Generated " + str(poster) + " ...")

    pdb.gimp_message("Generated Poster")

register(
	"python-fu-poster-script",
	"Poster Script",
	"This Poster Production Script",
	"Muddy Games", 
	"Muddy Games",
	"2016",
	"Poster Script",
	"RGB",
	[
    	(PF_IMAGE, "image", "Current Image", None),
    	(PF_DRAWABLE, "drawable", "Current Drawable", None),
    	(PF_LAYER, "layer", "Current Layer", None),
    	(PF_FILE, "background", "Poster Background", "background.png"),
    	(PF_FILE, "foreground", "Poster Foreground", "foreground.png"),
    	(PF_SLIDER, "foreground_alpha", "Foreground Transparency", 100, (0, 100, 1)),
    	(PF_FILE, "subject", "Poster Subject", "subject.png"),
    	(PF_SLIDER, "subject_alpha", "Subject Transparency", 100, (0, 100, 1)),
    	(PF_STRING, "title", "Title Text", "This is the Title"),
    	(PF_INT, "title_x", "Title Text X Position", 40),
    	(PF_INT, "title_y", "Title Text Y Position", 10),
    	(PF_STRING, "sub_title", "Subtitle Text", "This is the Subtitle"),
    	(PF_INT, "sub_title_x", "Subtitle Text X Position", 40),
    	(PF_INT, "sub_title_y", "Subtitle Text Y Position", 100),
        (PF_FILE, "poster", "Poster Filename", "poster.png")
	],
	[],
	poster_script,
	menu="<Image>/Filters")

main()