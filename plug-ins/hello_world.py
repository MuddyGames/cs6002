from gimpfu import *
from PIL import Image
from PIL import ImageFilter
import datetime
import sys

def hello_world(width, height, message):
    sys.stdout = open( 'c:\\temp\\out.txt', 'a')
    print(datetime.datetime.now())
    print(width)
    print(height)
    print(height)
    pdb.gimp_message('Hello World !....')
    pdb.gimp_message('Producing Image!....')

    im = Image.open("C:\\temp\\ProfilePicture.png")     # Place an image in an accessible folder
    pdb.gimp_message('Trying EMBOSS')
    out = im.convert('RGB')                             # Convert to RGB
    result = out.filter(ImageFilter.EMBOSS)
    pdb.gimp_message('Finished EMBOSS')
    result.save("C:\\temp\\copy_script.png")            # Copy of image saved

    pdb.gimp_message('Image Produced.....')
    print('Finished processing')

register(
    'python-fu-hello-world',                            # Plugin unique name
    'Hello World',                                      # Tooltip short name
    'This is a Hello World Plugin',                     # Tooltip long description
    'Muddy Games', 'Muddy Games', '2016',               # Creator, Copyright owner, released
    'Hello World',                                      # Menu name
    '*',                                                # Supported image color * <all>
    [
        (PF_INT, "max_width", "Maximum Width", 500),
        (PF_INT, "max_height", "Maximum Height", 500),
        (PF_STRING, "message", "Message to display", "hello"),
    ],                                                  # Input Parameters
    [],                                                 # Return Parameters
    hello_world,                                        # Method to call
    menu='<Image>/Filters/Render')               	# Menu entry location

main()