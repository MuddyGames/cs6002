<#
PowerShell script to launch GIMP. It also creates a directory for log file
#>

#Check if directory already exists
$log_Dir = "C:\temp"
$result = Test-Path $log_Dir

#If logging directory does not exist create it
if(!$result)
{
	New-Item -ItemType directory -Path $log_Dir
}

$application = "C:\Program Files\GIMP 2\bin\gimp-2.8.exe"
#Starting GIMP   
& $application --no-data --verbose --no-splash --console-messages --gimprc=".\config\gimprc"